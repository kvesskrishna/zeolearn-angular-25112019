import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  blogForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.blogForm = this.fb.group({
      title: ['', Validators.required],
      subject: ['Default', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      content: ['', Validators.required]
    });
    
  }
  get f() {
    return this.blogForm.controls;
  }
  submit() {
    this.blogForm = this.fb.group({
      title: ['1234']
    })
    console.log(this.f);
    console.log(this.blogForm)
    this.blogForm.value.title = 'test123';
    console.log(this.blogForm.value);
  }

}
