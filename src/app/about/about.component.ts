import { Title } from "@angular/platform-browser";
import { Component, OnInit } from "@angular/core";
import { MyServicesService } from "../my-services.service";
@Component({
  selector: "app-about",
  templateUrl: "./about.component.html",
  styleUrls: ["./about.component.css"]
})
export class AboutComponent implements OnInit {
  name = "";
  constructor(private myservice: MyServicesService, private title: Title) {}

  ngOnInit() {
    this.title.setTitle("About");
    this.name = this.myservice.getName();
  }
}
