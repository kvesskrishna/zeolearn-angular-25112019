import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { NavComponent } from './nav/nav.component';
import { ProductsComponent } from './products/products.component';
import { OldPriceDirective } from './directives/old-price.directive';
import { AddToCartBtnComponent } from './products/add-to-cart-btn/add-to-cart-btn.component';
import { DiscountPipe } from './pipes/discount.pipe';
import { ProductFilterPipe } from './pipes/product-filter.pipe';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseComponent } from './course/course.component';
import { SharedModule } from './shared/shared.module';
import { CategoryComponent } from './courses/category/category.component';
import { UsersComponent } from './users/users.component';
import { UserFilterPipe } from './pipes/user-filter.pipe';
import { UserComponent } from './user/user.component';
import { RegisterComponent } from './register/register.component';
import { BlogComponent } from './blog/blog.component';
import { MapDemoComponent } from './map-demo/map-demo.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    NavComponent,
    ProductsComponent,
    OldPriceDirective,
    AddToCartBtnComponent,
    DiscountPipe,
    ProductFilterPipe,
    AboutComponent,
    ContactComponent,
    HomeComponent,
    PageNotFoundComponent,
    CoursesComponent,
    CourseComponent,
    CategoryComponent,
    UsersComponent,
    UserFilterPipe,
    UserComponent,
    RegisterComponent,
    BlogComponent,
    MapDemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
