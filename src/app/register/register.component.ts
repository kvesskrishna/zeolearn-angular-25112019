import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private http: HttpClient) { }
  lname = '';
  fname = '';
  email = '';
  password = '';
  ngOnInit() {
  }
  onSubmit(values) {
    console.log(values);
    this.http.post('api end point', values);
    // console.log (this.fname, this.lname, this.email, this.password);
  }

}
