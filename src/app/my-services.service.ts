import { Injectable } from '@angular/core';
// import { HttpClient } from 'selenium-webdriver/http';
import { HttpClient } from '@angular/common/http';
// HttpClientModule
@Injectable({
  providedIn: 'root'
})
export class MyServicesService {

  constructor(private http: HttpClient) { }
  name = '';
  setName(name) {
    this.name = name;
  }
  getName() {
    return this.name;
  }
  getUsers() {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }
}
