import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseComponent } from './course/course.component';
import { TestComponent as TestFromAppComponent } from './test/test.component';
import { TestComponent as TestFromSharedComponent } from './shared/test/test.component';
import { SharedModule } from './shared/shared.module';
import { CategoryComponent } from './courses/category/category.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { RegisterComponent } from './register/register.component';
import { BlogComponent } from './blog/blog.component';
import { MapDemoComponent } from './map-demo/map-demo.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch:'full'},
  {path: 'home', component: HomeComponent},
  {path: 'about/krishna/in/2017', component: AboutComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'courses', component: CoursesComponent, children: [
    {path: 'category/:id', component: CategoryComponent}
  ]},
  {path: 'course/:id', component: CourseComponent},
  {path: 'test', component: TestFromSharedComponent},
  {path: 'users', component: UsersComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'user/:id', component: UserComponent},
  {path: 'map', component: MapDemoComponent},
  // {path: 'dashboard', loadChildren: './admin/admin.module#AdminModule'},
  { path: 'dashboard', loadChildren: () => import(`./admin/admin.module`).then(m => m.AdminModule) },

  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
