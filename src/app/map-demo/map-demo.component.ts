import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-demo',
  templateUrl: './map-demo.component.html',
  styleUrls: ['./map-demo.component.css']
})
export class MapDemoComponent implements OnInit {

  constructor() { }
  numbers = [1,2,3,4,5,6,7,8];
  ngOnInit() {
  }

  changenumbers(event) {
    console.log(event.target.value);
    let factor = event.target.value;
    this.numbers = this.numbers.map(number=>number*factor);
  }

}
