import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { courses } from 'src/assets/data/courses';
import { microsoftcourses } from 'src/assets/data/courses';
import { networkcourses } from 'src/assets/data/courses';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }
  mycourses: any[] = [];

  ngOnInit() {
    this.route.params.subscribe(params => {
      let param = params['id'];
      switch (param) {
        case '1':
          this.mycourses = courses;
          break;
          case '2':
            this.mycourses = microsoftcourses;
            break;
            case '3':
              this.mycourses = networkcourses;
              break;
      
        default:
          this.router.navigate(['undefined']);
          break;
      }
  
  });
  }

}
