import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productFilter'
})
export class ProductFilterPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    console.log(args[0])
    return value.filter(product=>product.name.includes(args[0]));
  }

}
