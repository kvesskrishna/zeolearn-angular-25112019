import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    console.log(args[0])
    let filtered: any[];
    // return value.filter(user=>user.name.toLowerCase().includes(args[0].toLowerCase()));
    return value.filter(row=> {
      return ((row.address.city.toLowerCase().includes(args[0].toLowerCase()))||(row.address.street.toLowerCase().includes(args[0].toLowerCase())))
    });
    // return filtered;
  }

}
