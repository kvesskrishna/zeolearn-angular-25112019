import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule} from '@angular/common/http';
import { FooterComponent } from './footer/footer.component';
import { TestComponent } from './test/test.component';
import { SharedRoutingModule } from './shared-routing.module';
import { MyServicesService } from '../my-services.service';
@NgModule({
  declarations: [FooterComponent, TestComponent],
  imports: [
    CommonModule,
    SharedRoutingModule,
    HttpClientModule
  ],
  exports: [
    FooterComponent,
    HttpClientModule
  ],
  providers: [MyServicesService]
})
export class SharedModule { }
