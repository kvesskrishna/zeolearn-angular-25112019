import { CartService } from "./services/cart.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
  providers: [CartService]
})
export class AppComponent implements OnInit {
  cartvalue = 0;
  constructor(private cart: CartService) {}
  ngOnInit() {
    this.cart.cart.subscribe(data => (this.cartvalue = data));
  }
}
