import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { OldPriceDirective } from '../directives/old-price.directive';
import { AddToCartBtnComponent } from './add-to-cart-btn/add-to-cart-btn.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  // @ViewChild(AddToCartBtnComponent, {static: false}) cartbtn;
  products:any[] = [];
  cols = 'col-3';
  msg = '';
  cartmessage = '';
  today = new Date();
  searchstr = '';
  numbers = [1,2,3,4,5,6,7,8,9,10];

  constructor() { }

  ngOnInit() {
    let evennumbers = this.numbers.filter(num=>num%2==0);
    console.log(evennumbers);
    this.products = [{
      id: 1,
      name: 'mobile',
      price: 1234,
      discount: 10,
      img: 'assets/img/mobile.png',
      description: 'blah blah'
    }, {
      id: 2,
      name: 'television',
      price: 12345,
      discount: 15,
      img: 'assets/img/tv.png',
      description: 'blah blah'

    }, {
      id: 3,
      name: 'oven',
      price: 1236,
      discount: 1,
      img: 'assets/img/oven.png',
      description: 'blah blah'

    }, {
      id: 4,
      name: 'shoes',
      price: 989,
      discount: 5,
      img: 'assets/img/shoes.png',
      description: 'blah blah'

    }];
  }

  // ngAfterViewInit(): void {
  //   this.msg = this.cartbtn.message;
  // }

  getFinalPrice(actualprice, discount) {
    return actualprice - discount/actualprice*100;
  }

  changeLayout() {

  }

  receiveMsg($event) {
    this.cartmessage = $event;
  }

}
