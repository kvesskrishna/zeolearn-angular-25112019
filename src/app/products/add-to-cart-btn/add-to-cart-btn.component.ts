import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
// import {  } from 'events';

@Component({
  selector: "app-add-to-cart-btn",
  templateUrl: "./add-to-cart-btn.component.html",
  styleUrls: ["./add-to-cart-btn.component.css"]
})
export class AddToCartBtnComponent implements OnInit {
  @Output() addtocartevent = new EventEmitter();
  message = "Please Buy Me";
  @Input() producttitle;
  constructor() {}

  ngOnInit() {}

  sendMessage() {
    this.addtocartevent.emit("Added To Cart");
  }
}
