import { CartService } from "./../services/cart.service";
import { Component, OnInit } from "@angular/core";
import { MyServicesService } from "../my-services.service";
import { Meta, Title } from "@angular/platform-browser";
// import { MyServicesService } from '../my-services.service';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
  providers: [MyServicesService]
})
export class HomeComponent implements OnInit {
  name = "";
  cartval = 0;
  constructor(
    private myservice: MyServicesService,
    private meta: Meta,
    private title: Title,
    private cart: CartService
  ) {}

  ngOnInit() {
    this.title.setTitle("Home");
    this.meta.addTag({
      name: "description",
      content: "How to use Angular meta service"
    });
  }

  setName() {
    // call a service which will set the name across the app
    this.myservice.setName(this.name);
  }

  updateCart() {
    this.cart.setCart(this.cartval);
  }
}
