import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import {courses} from '../../assets/data/courses';
@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }
  id: any;
  course: any;
  chapters: any;
  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    console.log(this.id);
    this.course = courses.filter(course=>course.id == this.id);
    console.log(this.course);
    if (this.course.length==0) {
      this.router.navigate(['/undefined']);
    }

    this.chapters = this.course[0].chapters;
    console.log(this.chapters);
  }

}
