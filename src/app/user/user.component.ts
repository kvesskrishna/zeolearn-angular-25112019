import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MyServicesService } from '../my-services.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private route: ActivatedRoute, private myservice: MyServicesService) { }
id='';
userinfo = '';
  ngOnInit() {
    
    this.route.params.subscribe(data => {
      this.id = data['id'];
      this.myservice.getUsers().subscribe(data=>{
        let users: any;
        users = data;
        this.userinfo = users.filter(user => user.id == this.id);
        console.log(this.userinfo)
      })
    })
  }

}
