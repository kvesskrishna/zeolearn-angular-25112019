import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appOldPrice]'
})
export class OldPriceDirective {

  constructor(private eleref: ElementRef) { 
    eleref.nativeElement.style.textDecoration = 'line-through';
  }
  @HostListener('mouseover') onMouseOver(){
    this.eleref.nativeElement.style.backgroundColor = 'yellow';
  }
  @HostListener('mouseleave') onMouseLeave(){
    this.eleref.nativeElement.style.backgroundColor = 'white';
  }
  @HostListener('click') onClick(){
    alert('You clicked old price');
    // this.eleref.nativeElement.style.backgroundColor = 'yellow';
  }


}
