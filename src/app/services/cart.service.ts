import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CartService {
  private cartSource = new BehaviorSubject(0);
  cart = this.cartSource.asObservable();

  constructor() {}

  setCart(value) {
    // this.cart = value;
    this.cartSource.next(value);
    console.log(this.cart);
  }
}
