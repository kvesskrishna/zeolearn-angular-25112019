import { Component, OnInit } from '@angular/core';
import { MyServicesService } from '../my-services.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private userservice: MyServicesService) { }

  users: any = [];
  search = '';

  ngOnInit() {
    this.userservice.getUsers().subscribe(
      data => {
      console.log(data);
      this.users = data;
    }, 
    err => {console.log(err)}
    );
  }

}
