export const courses: any[] = [
    {
        id: 1, 
        title:'HTML', 
        chapters: [
            {
                id: 1, 
                title: 'Tags'
            }]
    },
    {
        id: 2, 
        title:'CSS', 
        chapters: [
            {
                id: 1, 
                title: 'Text'
            }]
    },
    {
        id: 3, 
        title:'JS', 
        chapters: [
            {
                id: 1, 
                title: 'Callbacks'
            }]
    },
    {
        id: 4, 
        title:'Angular', 
        chapters: [
            {
                id: 1, 
                title: 'Routing'
            },
            {
                id: 2, 
                title: 'Services'
            }

        ]
    },
];
export const microsoftcourses: any[] = [
    {
        id: 1, 
        title:'Word', 
        chapters: [
            {
                id: 1, 
                title: 'Tags'
            }]
    },
    {
        id: 2, 
        title:'Windows', 
        chapters: [
            {
                id: 1, 
                title: 'Text'
            }]
    },
    {
        id: 3, 
        title:'Powerpoint', 
        chapters: [
            {
                id: 1, 
                title: 'Callbacks'
            }]
    },
    {
        id: 4, 
        title:'Azure', 
        chapters: [
            {
                id: 1, 
                title: 'Routing'
            },
            {
                id: 2, 
                title: 'Services'
            }

        ]
    },
];
export const networkcourses: any[] = [
    {
        id: 1, 
        title:'Network Securty', 
        chapters: [
            {
                id: 1, 
                title: 'Tags'
            }]
    },
    {
        id: 2, 
        title:'Ethical Hacking', 
        chapters: [
            {
                id: 1, 
                title: 'Text'
            }]
    },
    {
        id: 3, 
        title:'Cisco', 
        chapters: [
            {
                id: 1, 
                title: 'Callbacks'
            }]
    },
    {
        id: 4, 
        title:'VPN', 
        chapters: [
            {
                id: 1, 
                title: 'Routing'
            },
            {
                id: 2, 
                title: 'Services'
            }

        ]
    },
];
// export default courses;